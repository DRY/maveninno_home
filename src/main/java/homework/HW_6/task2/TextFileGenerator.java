package homework.HW_6.task2;

import java.io.*;
import java.util.Locale;
import java.util.Random;

public class TextFileGenerator {
    /**
     * Создать генератор текстовых файлов, работающий по следующим правилам:
     *
     * Предложение состоит из 1<=n1<=15 слов. В предложении после произвольных слов могут находиться запятые.
     * Слово состоит из 1<=n2<=15 латинских букв
     * Слова разделены одним пробелом
     * Предложение начинается с заглавной буквы
     * Предложение заканчивается (.|!|?)+" "
     * Текст состоит из абзацев. в одном абзаце 1<=n3<=20 предложений. В конце абзаца стоит разрыв строки и перенос каретки.
     * Есть массив слов 1<=n4<=1000. Есть вероятность probability вхождения одного из слов этого массива в следующее предложение (1/probability).
     *
     * Необходимо написать метод getFiles(String path, int n, int size, String[] words, int probability), который создаст n файлов размером size в каталоге path.
     * words - массив слов, probability - вероятность.
     */

    private static int probability = 0;
    private static String[] arrayWords;

    public static void main(String[] args) {
        System.out.println(getWord(true));
    }

    public static void getFiles(String path, int n, int size, String[] words, int probability) throws IOException {
        /**
         * метод getFiles(String path, int n, int size, String[] words, int probability), который создаст n файлов размером size в каталоге path.
         * words - массив слов, probability - вероятность.
         */
        TextFileGenerator.probability = probability;
        arrayWords = words;
        for (int i = 0; i < n; i++) {
            File file = new File(path + "\\File" + i + ".txt");
            file.createNewFile();
            try (RandomAccessFile f = new RandomAccessFile(file, "rw")) {
                while (f.length() <= size) {
                    f.writeBytes(getParagraph());
                }
                System.out.println(f.length());
                f.setLength(size);
                System.out.println(f.length());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static String getWord(boolean firstWord){
        /**
         * Возвращает случайное слово, параметром можно задать первую заглавную букву.
         * Слово состоит из 1<=n2<=15 латинских букв.
         */
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower = upper.toLowerCase(Locale.ROOT);
        StringBuilder randWord = new StringBuilder();
        Random rnd = new Random();
        int index;
        if (rnd.nextInt(100) < probability) {
            index = (int) (rnd.nextFloat() * arrayWords.length);
//            System.out.println("из arrayWords - " + arrayWords[index]);
            if (firstWord)
                return arrayWords[index].substring(0,1).toUpperCase() + arrayWords[index].substring(1);
            return arrayWords[index];
        }
        int length = rnd.nextInt(15) + 1;
        while (randWord.length() < length) {
            index = (int) (rnd.nextFloat() * upper.length());
            if (randWord.length() == 0 & firstWord) {
                randWord.append(upper.charAt(index));
            }
            else {
                randWord.append(lower.charAt(index));
            }
        }
        return randWord.toString();
    }

    static String getSentence(){
        /**
         * Возвращает предложение из случайных слов.
         * Предложение состоит из 1<=n1<=15 слов. В предложении после произвольных слов могут находиться запятые.
         * Слова разделены одним пробелом
         * Предложение начинается с заглавной буквы
         * Предложение заканчивается (.|!|?)+" "
         */
        String mark = ".!?";
        StringBuilder randSentence = new StringBuilder();
        Random rnd = new Random();
        int length = rnd.nextInt(15);
        int index;
        for (int i = 0; i <= length; i++) {
            if (i == 0)
                randSentence.append(getWord(true));
            else
                randSentence.append(getWord(false));
            if (i == length) {
                index = (int) (rnd.nextFloat() * mark.length());
                randSentence.append(mark.charAt(index));
            }
            else {
                if (rnd.nextBoolean())
                    randSentence.append(",");
            }
            randSentence.append(" ");
        }
        return randSentence.toString();
    }

    static String getParagraph(){
        /**
         * Возвращает абзац случайных предложений. В конце абзаца стоит разрыв строки и перенос каретки.
         */
        StringBuilder randParagraph = new StringBuilder();
        Random rnd = new Random();
        int length = rnd.nextInt(20);
        for (int i = 0; i <= length; i++) {
            randParagraph.append(getSentence());
        }
        randParagraph.append(System.lineSeparator());
        return randParagraph.toString();
    }

    static String[] generateArrayWords(int count) {
        /**
         * Генерирует массив случайных слов.
         */
        String[] str = new String[count];
        for (int i = 0; i < str.length; i++)
            str[i] = getWord(false);
        return str;
    }

}
