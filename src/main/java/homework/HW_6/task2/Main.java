package homework.HW_6.task2;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String[] words = TextFileGenerator.generateArrayWords(1000);
        TextFileGenerator.getFiles("src\\homework\\HW_6\\task2\\Files",2,4096, words, 10);
    }
}
