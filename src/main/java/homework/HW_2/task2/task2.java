package homework.HW_2.task2;
import java.util.Random;

public class task2 {
    /**
     * Составить программу, генерирующую N случайных чисел. Для каждого числа k вычислить квадратный корень q.
     * Если квадрат целой части q числа равен k, то вывести это число на экран.
     * Предусмотреть что первоначальные числа могут быть отрицательные, в этом случае генерировать исключение.
     */
    public static void main(String[] args) {
        int N = 50000;
        int[] k = getArray(N);
        double[] q = new double[N];
        for (int i = 0; i < k.length; i++){
            try {
                if (k[i] < 0) {
                    throw new MyException();
                }
                else {
                    q[i] = Math.sqrt((double) k[i]);
                    if (Math.pow((int)q[i],2) == k[i]){
                        System.out.println(k[i]);
                    }
                }
            }
            catch (MyException e){
                e.addLog(k[i]);
            }

        }
        MyException.getLogException();
    }

    static int[] getArray(int N){
        /**
         * Получить N случайных чисел
         */
        int[] a = new int[N];
        Random random = new Random();
        for (int i = 0; i < N; i++){
            a[i] = random.nextInt();
        }
        return a;
    }

    static class MyException extends RuntimeException{
        /**
         * Исключение для первоначальных отрицательных чисел
         */
        static String str = "Числа, от которых не удалось вернуть квадратный корень:\n";
        public MyException() {

        }

        static void addLog(int k){
            str = str + k + "; ";
        }

        static void getLogException(){
            System.out.println(str);
        }
    }
}
