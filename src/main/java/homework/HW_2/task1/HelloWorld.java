package homework.HW_2.task1;

public class HelloWorld {
    /**
     * Написать программу ”Hello, World!”. В ходе выполнения программы она должна выбросить исключение и завершиться с ошибкой.
     * @param args
     */
    public static void main(String[] args) {
        //testNPE();
        //testAIOOBE();
        myThrow();
    }

    static void testNPE(){
        /**
         * Смоделировав ошибку «NullPointerException»
         */
        String str = null;
        if (str.equals("test")){
            System.out.println(str);
        }
    }

    static void testAIOOBE() {
        /**
         * Смоделировав ошибку «ArrayIndexOutOfBoundsException»
         */
        int a[] = new int[1];
        System.out.println(a[2]);
    }

    static void myThrow(){
        /**
         * Вызвав свой вариант ошибки через оператор throw
         */
        throw new ArithmeticException();
    }
}
