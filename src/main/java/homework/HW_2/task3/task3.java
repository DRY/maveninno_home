package homework.HW_2.task3;

import java.util.Locale;
import java.util.Random;

public class task3 {
    /**
     * Дан массив объектов Person. Класс Person характеризуется полями age (возраст, целое число 0-100),
     * sex (пол – объект класса Sex со строковыми константами внутри MAN, WOMAN), name (имя - строка).
     * Создать два класса, методы которых будут реализовывать сортировку объектов.
     * Предусмотреть единый интерфейс для классов сортировки. Реализовать два различных метода сортировки этого массива по правилам:
     * - первые идут мужчины
     * - выше в списке тот, кто более старший
     * - имена сортируются по алфавиту
     *
     * Программа должна вывести на экран отсортированный список и время работы каждого алгоритма сортировки.
     * Предусмотреть генерацию исходного массива (10000 элементов и более).
     * Если имена людей и возраст совпадают, выбрасывать в программе пользовательское исключение.
     * @param args
     */
    public static void main(String[] args) {
        Person[] pa = CreatePersons(10000);
        new BubbleSort(pa);
        new InsertionSort(pa);
    }

    enum sex {MAN, WOMAN}

    static Person[] CreatePersons(int N) {
        Person[] p = new Person[N];
        for (int i = 0; i < N; i++) {
            p[i] = getRandPerson();
        }
        return p;
    }

    static Person getRandPerson() {
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower = upper.toLowerCase(Locale.ROOT);
        StringBuilder randName = new StringBuilder();
        Random rnd = new Random();
        int length = rnd.nextInt(8) + 2;
        int index = (int) (rnd.nextFloat() * upper.length());
        randName.append(upper.charAt(index));
        while (randName.length() < length) {
            index = (int) (rnd.nextFloat() * lower.length());
            randName.append(lower.charAt(index));
        }
        sex randSex = sex.MAN;
        if (rnd.nextInt(2) == 1) {
            randSex = sex.WOMAN;
        }
        Person p = new Person(randName.toString(), randSex, rnd.nextInt(100));
        return p;
    }

    static class Person {
        int age;
        sex sex;
        String name;
        
        public Person(String name, sex sex, int age){
            this.name = name;
            this.sex = sex;
            if (age >= 0 & age <= 100){
                this.age = age;
            }
        }

        sex getSex(){
            return this.sex;
        }

        void printPerson(){
            System.out.println(this.name + " - " + this.sex.name() + " - " + this.age);
        }
    }

    static class BubbleSort implements Sort {
        public BubbleSort(Person[] p){
            System.out.println("BubbleSort:");
            long start = System.nanoTime();
            p = sortName(p);
            p = sortAge(p);
            p = sortSex(p);
//            for (int i = 0; i < p.length; i++) {
//                p[i].printPerson();
//            }
            long finish = System.nanoTime();
            System.out.println(String.format("BubbleSort - %,2f",(float)(finish - start)/1000000000) + " seconds");
        }

        static Person[] sortSex(Person[] p){
            boolean needIteration = true;
            while (needIteration) {
                needIteration = false;
                for (int i = 1; i < p.length; i++) {
                    if (p[i].getSex() == sex.MAN & p[i-1].getSex() == sex.WOMAN) {
                        swap(p, i, i-1);
                        needIteration = true;
                    }
                }
            }
            return p;
        }

        static Person[] sortAge(Person[] p){
            boolean needIteration = true;
            while (needIteration) {
                needIteration = false;
                for (int i = 1; i < p.length; i++) {
                    if (p[i].age > p[i - 1].age) {
                        swap(p, i, i - 1);
                        needIteration = true;
                    }
                    try {
                        if (p[i].age == p[i -1].age & p[i].name == p[i-1].name) {
                            throw new MyException();
                        }
                    }
                    catch (MyException e) {
                        System.out.println("Совпадение имени и возраста:");
                        p[i].printPerson();
                        p[i-1].printPerson();
                    }
                }
            }
            return p;
        }

        static Person[] sortName(Person[] p){
            boolean needIteration = true;
            while (needIteration) {
                needIteration = false;
                for (int i = 1; i < p.length; i++) {
                    if (p[i].name.compareTo(p[i - 1].name) < 0) {
                        swap(p, i, i - 1);
                        needIteration = true;
                    }
                }
            }
            return p;
        }

        private static void swap(Person[] p, int i1, int i2) {
            Person tmp = p[i1];
            p[i1] = p[i2];
            p[i2] = tmp;
        }
    }

    static class InsertionSort implements Sort {
        public InsertionSort(Person[] p){
            System.out.println("InsertionSort:");
            long start = System.nanoTime();
            p = sortName(p);
            p = sortAge(p);
            p = sortSex(p);
//            for (int i = 0; i < p.length; i++) {
//                p[i].printPerson();
//            }
            long finish = System.nanoTime();
            System.out.println(String.format("SelectionSort - %,2f",(float)(finish - start)/1000000000) + " seconds");
        }

        static Person[] sortSex(Person[] p) {
            for (int i = 0; i < p.length; i++) {
                Person value = p[i];
                int j = i - 1;
                for (; j >= 0; j--) {
                    if (value.getSex() == sex.MAN & p[j].getSex() == sex.WOMAN) {
                        p[j+1] = p[j];
                    } else {
                        break;
                    }
                }
                p[j+1] = value;
            }
            return p;
        }

        static Person[] sortAge(Person[] p){
            for (int i = 0; i < p.length; i++) {
                Person value = p[i];
                int j = i - 1;
                for (; j >= 0; j--) {
                    if (value.age > p[j].age) {
                        p[j+1] = p[j];
                    } else {
                        break;
                    }
                    try {
                        if (value.age == p[j].age & value.name == p[j].name) {
                            throw new MyException();
                        }
                    }
                    catch (MyException e) {
                        System.out.println("Совпадение имени и возраста:");
                        value.printPerson();
                        p[j].printPerson();
                    }
                }
                p[j+1] = value;
            }
            return p;
        }

        static Person[] sortName(Person[] p){
            for (int i = 0; i < p.length; i++) {
                Person value = p[i];
                int j = i - 1;
                for (; j >= 0; j--) {
                    if (value.name.compareTo(p[j].name) < 0) {
                        p[j+1] = p[j];
                    } else {
                        break;
                    }
                }
                p[j+1] = value;
            }
            return p;
        }
    }

    public interface Sort{
        static Person[] sortSex(Person[] p) {
            return p;
        }

        static Person[] sortAge(Person[] p){
            return p;
        }

        static Person[] sortName(Person[] p){
            return p;
        }
    }

    static class MyException extends RuntimeException{
        /**
         * Исключение для первоначальных отрицательных чисел
         */
        public MyException() {

        }
    }

}
