package homework.HW_3.task3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MathBox<T extends Number> extends ObjectBox{

    public static void main(String[] args) {
        Number[] arr = {1,2,3,4.4,4.4,9};
        MathBox mb = new MathBox<>(arr);
        //Number sum = mb.summator();
        //System.out.println("Sum = " + sum);
        //mb.splitter(2);
        mb.deleteObject(4);
        System.out.println(mb.dump());
    }

    private Set<T> setArr;

    public MathBox(T[] arr){
        super(arr);
    }

    public Number summator(){
        Number sum = 0;
        for (Number n : setArr){
            sum = sum.doubleValue() + n.doubleValue();
        }
        return sum;
    }

    public void splitter(T sp){
        Set<Double> tempSetArr = new HashSet<>();
        for (Number n : setArr){
            tempSetArr.add(n.doubleValue()/sp.doubleValue());
        }
        setArr = (Set<T>) tempSetArr;
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        for (Number n : setArr){
            str.append(n);
            str.append(", ");
        }
        str.deleteCharAt(str.length()-2);
        return str.toString();
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        for (Number n : setArr){
            result = prime * result + n.intValue();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) {
            System.out.println(1);
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()){
            System.out.println(2);
            return false;
        }
        MathBox t = (MathBox) obj;
        System.out.println(setArr);
        System.out.println(t.setArr);
        if (setArr != t.setArr) {
            System.out.println(3);
            return false;
        }
        return true;
    }
}
