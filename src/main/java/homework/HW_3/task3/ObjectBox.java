package homework.HW_3.task3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ObjectBox<T extends Object> {
    private Set<T> setObj;

    public static void main(String[] args) {
        Object[] arr = {1,2,3,4.4,4.4,9};
        ObjectBox mb = new ObjectBox<>(arr);
        mb.addObject(10);
        mb.deleteObject(1);
        System.out.println(mb.dump());
    }

    public ObjectBox(T[] arr){
        setObj = new HashSet<T>(Arrays.asList(arr));
    }

    public void addObject(T obj) {
        setObj.add(obj);
    }

    public void deleteObject(T obj) {
        for (Object n : setObj){
            if (n == obj) {
                setObj.remove(n);
                break;
            }
        }
    }

    public String dump() {
        StringBuilder str = new StringBuilder();
        for (Object n : setObj){
            str.append(n);
            str.append(", ");
        }
        str.deleteCharAt(str.length()-2);
        return str.toString();
    }
}
