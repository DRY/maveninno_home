package homework.HW_5.entity;

import java.util.Locale;
import java.util.Random;

public class Person {
    private int age;
    private sex personSex;
    private String name;

    public enum sex {MAN, WOMAN}

    public Person(String name, sex personSex, int age){
        this.name = name;
        this.personSex = personSex;
        if (age >= 0 & age <= 100){
            this.age = age;
        }
    }

    public sex getSex(){
        return this.personSex;
    }
    public int getAge(){
        return this.age;
    }
    public String getName(){
        return this.name;
    }

    public void printPerson(){
        /**
         * вывод информации о хозяине
         */
        System.out.println(this.name + " - " + this.personSex.name() + " - " + this.age);
    }

    public static Person getRandPerson() {
        /**
         * получение случайного хозяина
         */
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower = upper.toLowerCase(Locale.ROOT);
        StringBuilder randName = new StringBuilder();
        Random rnd = new Random();
        int length = rnd.nextInt(8) + 2;
        int index = (int) (rnd.nextFloat() * upper.length());
        randName.append(upper.charAt(index));
        while (randName.length() < length) {
            index = (int) (rnd.nextFloat() * lower.length());
            randName.append(lower.charAt(index));
        }
        sex randSex = sex.MAN;
        if (rnd.nextInt(2) == 1) {
            randSex = sex.WOMAN;
        }
        Person p = new Person(randName.toString(), randSex, rnd.nextInt(100));
        return p;
    }

    public boolean equals(Person p){
        /**
         * сравнение хозяев
         */
        if (this.age == p.age && this.name.equals(p.name) && this.personSex == p.personSex)
            return true;
        return false;
    }
}
