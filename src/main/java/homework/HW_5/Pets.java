package homework.HW_5;

import homework.HW_5.entity.Person;
import homework.HW_5.entity.Pet;

import java.util.*;

public class Pets {
    /**
     * Разработать программу – картотеку домашних животных. У каждого животного есть уникальный идентификационный номер,
     * кличка, хозяин (объект класс Person с полями – имя, возраст, пол), вес.
     *
     * Реализовать:
     * метод добавления животного в общий список (учесть, что добавление дубликатов должно приводить к исключительной ситуации)
     * поиск животного по его кличке (поиск должен быть эффективным)
     * изменение данных животного по его идентификатору
     * вывод на экран списка животных в отсортированном порядке. Поля для сортировки –  хозяин, кличка животного, вес.
     */

    private Set<Pet> pets;

    public static void main(String[] args) {
        Pets petCard = new Pets(Pet.CreatePets(10));
        Pet myPet = new Pet("Dog", new Person("Pol", Person.sex.MAN, 25), 15);
        Pet myPet1 = new Pet("Dog", new Person("Pol", Person.sex.MAN, 25), 14);
        petCard.addPets(myPet);
        petCard.addPets(myPet);
//        Pet searchPet = petCard.searchPet("doz");
//        searchPet.showPet();
//        petCard.changeNicknamePet(searchPet.getId(),"Dow");
//        petCard.changeOwnerPet(searchPet.getId(), new Person("Poz", Person.sex.MAN, 24));
//        petCard.changeWeightPet(searchPet.getId(), 14.2f);
//        searchPet.showPet();
        petCard.showPets();
    }

    public Pets(Pet[] pets) {
        this.pets = new HashSet<>(Arrays.asList(pets));
    }

    void addPets(Pet p) {
        /**
         * метод добавления животного в общий список
         */
        boolean flag = false;
        for (Pet pet : pets){
            try {
                if (pet.getId().equals(p.getId()) && pet.getNickname().equals(p.getNickname()) && pet.getWeight() == p.getWeight()) {
                    flag = true;
                    throw new Exception();
                }
            }
            catch (Exception e) {
                System.out.println("Животное уже есть в картотеке");
            }
        }
        if (!flag){
            pets.add(p);
        }
    }

    Pet searchPet(String nickname) {
        /**
         * поиск животного по его кличке
         */
        for (Pet p : pets){
            if(p.getNickname().toUpperCase().equals(nickname.toUpperCase()))
                return p;
        }
        return null;
    }

    void changeNicknamePet (UUID id, String nickname){
        /**
         * изменение клички животного по его идентификатору
         */
        Pet p = getPet(id);
        p.setNicknamePet(nickname);
    }

    void changeOwnerPet (UUID id, Person owner){
        /**
         * изменение хозяина животного по его идентификатору
         */
        Pet p = getPet(id);
        p.setOwnerPet(owner);
    }

    void changeWeightPet (UUID id, float weight){
        /**
         * изменение веса животного по его идентификатору
         */
        Pet p = getPet(id);
        p.setWeightPet(weight);
    }

    Pet getPet(UUID id){
        /**
         * метод получения питомца по уникальному идентиифкатору
         */
        for (Pet p : pets){
            if (p.getId().equals(id))
                return p;
        }
        return null;
    }

    void showPets(){
        /**
         * вывод на экран списка животных в отсортированном порядке. Поля для сортировки –  хозяин, кличка животного, вес.
         */
        Pet[] p = new Pet[pets.size()]; //= (Pet[]) pets.toArray();
        int ind = 0;
        for (Pet pet : pets) {
            p[ind] = pet;
            ind++;
        }
        printPets(p);
        for (int i= 0; i < p.length; i++) {
            Pet value = p[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (comparePerson(value.getOwner(), p[j].getOwner()))
                    p[j+1] = p[j];
                else
                    if (value.getOwner().equals(p[j].getOwner())) {
                        if (value.getNickname().compareTo(p[j].getNickname()) < 0)
                            p[j+1] = p[j];
                        else
                        if (value.getWeight() > p[j].getWeight())
                            p[j+1] = p[j];
                        else
                            break;
                    }
                    else
                        break;
            }
            p[j+1] = value;
        }
        printPets(p);

    }

    boolean comparePerson(Person p1, Person p2) {
        /**
         * метод сравнения хозяев для сортировки
         */
        if (p1.getSex() == Person.sex.WOMAN && p2.getSex() == Person.sex.MAN)
            return false;
        if (p1.getSex() == Person.sex.MAN && p2.getSex() == Person.sex.WOMAN)
            return true;
        else {
            if (p1.getAge() > p2.getAge())
                return true;
            else {
                if (p1.getAge() == p2.getAge()) {
                    if (p1.getName().compareTo(p2.getName()) < 0)
                        return true;
                }
            }
        }
        return false;
    }

    void printPets(Pet[] p){
        /**
         * метод вывода питомцев на экран
         */
        for (int i= 0; i < p.length; i++) {
            System.out.println("Хозяин: " + p[i].getOwner().getName() + " " + p[i].getOwner().getSex() + " " + p[i].getOwner().getAge() +
                    "; кличка питомца - " + p[i].getNickname() + "; вес - " + p[i].getWeight() + " кг.");
        }
        System.out.println();
    }
}
