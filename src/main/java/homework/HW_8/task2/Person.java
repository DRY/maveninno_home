package homework.HW_8.task2;

import java.io.Serializable;

public class Person implements Serializable {

    private int age;
    private sex personSex;
    private String name;
    private float height;
    public Pet pet;
    private String country;

    public enum sex {MAN, WOMAN}

    public Person() {

    }

    public Person(String name, sex sex, int age, float height, String country, Pet pet){
        this.name = name;
        this.personSex = sex;
        this.age = age;
        this.height = height;
        this.country = country;
        this.pet = pet;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public sex getSex(){
        return this.personSex;
    }
    public void setSex(sex sex){
        this.personSex = sex;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public float getHeight(){
        return this.height;
    }
    public void setHeight(float height){
        this.height = height;
    }
    public String getCountry(){
        return this.country;
    }

    public void printPerson(){
        /**
         * вывод информации о хозяине
         */
        System.out.println(this.name + " - " + this.personSex.name() + " - " + this.age + " - " + this.pet.getNickname() + " - " + this.pet.getWeight());
    }
}
