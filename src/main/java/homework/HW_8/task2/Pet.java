package homework.HW_8.task2;

public class Pet {

    private String nickname;
    private float weight;

    public Pet(){}
    public Pet(String nickname, float weight){
        this.nickname = nickname;
        this.weight = weight;
    }

    public void setNicknamePet (String nickname){
        this.nickname = nickname;
    }

    public void setWeightPet (float weight){
        this.weight = weight;
    }

    public String getNickname(){
        return this.nickname;
    }

    public float getWeight(){
        return this.weight;
    }

    public void showPet(){
        /**
         * вывод информации о питомце
         */
        System.out.println("кличка питомца - " + this.nickname + "; вес - " + this.weight + " кг.");
    }
}
