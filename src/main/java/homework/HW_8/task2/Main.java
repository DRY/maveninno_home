package homework.HW_8.task2;

import java.io.*;
import java.lang.reflect.Field;

public class Main {

    /**
     * Необходимо разработать класс, реализующий следующие методы:
     *
     * void serialize (Object object, String file);
     *
     * Object deSerialize(String file);
     *
     * Методы выполняют сериализацию объекта Object в файл file и десериализацию объекта из этого файла.
     * Обязательна сериализация и десериализация "плоских" объектов (все поля объекта - примитивы, или String).
     *
     * Предусмотреть работу c любыми типами полей (полями могут быть ссылочные типы).
     */

    public static void main(String[] args) throws FileNotFoundException {
        Person p = new Person("Adsdagfdhbaa", Person.sex.MAN, 23, 54, "England", new Pet("Bgfgdor", 11));
        p.printPerson();
//        serialize(p, "src\\main\\java\\homework\\HW_8\\task2\\person.bin");
        Person newP = (Person) deSerialize("src\\main\\java\\homework\\HW_8\\task2\\person.bin");
        newP.printPerson();
    }

    private static void serialize(Object object, String file) {
        /**
         * Метод выполняет сериализацию объекта Object в файл file
         */
        Person p = (Person) object;
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeInt(p.getAge());
            dos.writeUTF(p.getSex().name());
            dos.writeUTF(p.getName());
            dos.writeFloat(p.getHeight());
            dos.writeUTF(p.getCountry());
            dos.writeUTF(p.pet.getNickname());
            dos.writeFloat(p.pet.getWeight());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Object deSerialize(String file) {
        /**
         * Метод выполняет десериализацию Object из file
         */
        Person p = new Person();
        Pet pet = new Pet();
        Class personClass = p.getClass();
        Class petClass = pet.getClass();
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            Field ageField = personClass.getDeclaredField("age");
            ageField.setAccessible(true);
            Field personSexField = personClass.getDeclaredField("personSex");
            personSexField.setAccessible(true);
            Field nameField = personClass.getDeclaredField("name");
            nameField.setAccessible(true);
            Field heightField = personClass.getDeclaredField("height");
            heightField.setAccessible(true);
            Field countryField = personClass.getDeclaredField("country");
            countryField.setAccessible(true);
            Field petField = personClass.getDeclaredField("pet");
            petField.setAccessible(true);
            Field nicknameField = petClass.getDeclaredField("nickname");
            nicknameField.setAccessible(true);
            Field weightField = petClass.getDeclaredField("weight");
            weightField.setAccessible(true);

            ageField.setInt(p, dis.readInt());
            personSexField.set(p, Person.sex.valueOf(dis.readUTF()));
            nameField.set(p, dis.readUTF());
            heightField.setFloat(p, dis.readFloat());
            countryField.set(p, dis.readUTF());
            nicknameField.set(pet, dis.readUTF());
            weightField.setFloat(pet, dis.readFloat());
            petField.set(p, pet);

        } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return p;
    }
}