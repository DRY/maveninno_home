package homework.HW_8.task1;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 22012L;
    @JsonProperty("age")
    private int age;
    @JsonProperty("sex")
    private String sex;
    @JsonProperty("name")
    private String name;
    @JsonProperty("height")
    private Double height;

    public Person() {

    }

    public Person(String name, String sex, int age, double height){
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.height = height;
    }

    public int getAge(){
        return this.age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public String getSex(){
        return this.sex;
    }
    public void setSex(String sex){
        this.sex = sex;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Double getHeight(){
        return this.height;
    }
    public void setHeight(Double height){
        this.height = height;
    }

    public void printPerson(){
        /**
         * вывод информации о хозяине
         */
        System.out.println(this.name + " - " + this.sex + " - " + this.age);
    }
}
