package homework.HW_8.task1;

import java.io.*;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;

public class Main {

    /**
     * Необходимо разработать класс, реализующий следующие методы:
     *
     * void serialize (Object object, String file);
     *
     * Object deSerialize(String file);
     *
     * Методы выполняют сериализацию объекта Object в файл file и десериализацию объекта из этого файла.
     * Обязательна сериализация и десериализация "плоских" объектов (все поля объекта - примитивы, или String).
     */

    public static void main(String[] args) {
        Person p = new Person("Aabaa", "MAN", 25, 65.5);
        p.printPerson();
//        serialize(p, "src\\main\\java\\homework\\HW_8\\task1\\person.json");
        Person newP = (Person) deSerialize("src\\main\\java\\homework\\HW_8\\task1\\person.json");
        newP.printPerson();
    }

    private static void serialize(Object object, String file) {
        /**
         * Метод выполняет сериализацию объекта Object в файл file
         */
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            mapper.writeValue(new File(file), object);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Object deSerialize(String file) {
        /**
         * Метод выполняет десериализацию Object из file
         */
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(new File(file), Person.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
